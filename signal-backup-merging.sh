#!/bin/bash

which ~/.cargo/bin/signal-backup-decode 2>/dev/null || exit 1
which sqlite3 2>/dev/null || exit 2
which sqldiff 2>/dev/null || exit 3
which wc 2>/dev/null || exit 4
which diff 2>/dev/null || exit 5
which /usr/share/fslint/fslint/findbl 2>/dev/null || exit 6

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
NC='\033[0m' # No Color
RED='\033[0;31m'
YELLOW='\033[1;33m'
BLUE='\033[1;34m'
PURPLE='\033[1;35m'

preferPatch=1
preferPatchMin1=$((preferPatch-1))
preferSize=${preferPatchMin1#-} # oposite of preferPatch
testdb="/tmp/test_diff_applying.db"

extract() {
  extracted="$(basename "$1")"
  extracted="${extracted%.backup}"

  if [ -d "$extracted" ]; then
    echo "ERROR: Target folder already exists! $extracted for $1"
    exit 7
  fi

  if [[ "$extracted" == "signal-2018-01-01-00-00-00" ]]; then
    ~/.cargo/bin/signal-backup-decode "$1" --password 000000000000000000000000000000
  elif [[ "$extracted" < "signal-2019-01-01-00-00-00" ]]; then
    ~/.cargo/bin/signal-backup-decode "$1" --password 000000000000000000000000000001
  elif [[ "$extracted" < "signal-2020-01-01-00-00-00" ]]; then
    ~/.cargo/bin/signal-backup-decode "$1" --password 000000000000000000000000000002
  else
    ~/.cargo/bin/signal-backup-decode "$1" --password 000000000000000000000000000003
  fi
}

restructure_backup() {
  bak="$1"
  newavatar="$bak/${bak/signal-/avatar_}"
  mv --verbose --no-clobber "$bak/avatar" "$newavatar"
  mv --verbose --no-clobber "$bak/signal_backup.db" "$bak/${bak/signal-/signal_backup_}.db"

  # restructure backup to have the prefered lexical order: : we want the attachment folder to link to the stickers and not the other way round. Also we want the avatars to link to stickers and attachments (and not the other way round), and since fslint/findup -s keeps the alphabetically first one as master and replaces all later duplicates with links to that one:"
  mv --verbose --no-clobber "$bak/sticker" "$bak/1_sticker"
  mv --verbose --no-clobber "$bak/attachment" "$bak/2_attachment"

  if [ -d "$bak/2_attachment/" ]; then
    for file in "$bak/2_attachment/"*; do
      base=${file##*/}
      time=${base%%_*}
      length=${#time}
      epoch=${time:0:(($length-3))}
      date=$(date -d @$epoch +%F_%H%M-%S)
      subsec=${time: -3}
      idAndExt=${base##*_}
      newname="$date,$subsec#$idAndExt"
      mv --no-clobber "$file" "$bak/2_attachment/$newname"
    done
  fi
}

convert_dups_to_symlinks_and_check() {
  echo "now we convert all duplicates into absolute hardlinks (to convert them into relative links in a second step)"
  /usr/share/fslint/fslint/findup -s "$1"

  echo "step 1 done, now we convert the absolute links into relative links"
  # every file sent / recieved multiple times + every sticker sent/recieved

  # every sticker set seems to have one of the stickers duplicated as header
  if [ $kept_avatar -eq 1 ];then
    # avatars mostly remain the same, but we keep changed folders to be able to see their history
    folders=("$1/1_sticker" "$1/2_attachment" "$1/$(basename ${newavatar})")
  else
    folders=("$1/1_sticker" "$1/2_attachment")
  fi
  for folder in "${folders[@]}"; do
    echo "converting absolute symlinks to relative ones in: $folder"
    if [ -d "$folder" ];then
      cd "$folder"
      find . -type l -exec ln -sfr {} . \;
      cd "$pwd"
    else
      echo "WARNING: folder doesn't exist: $folder"
    fi
  done

  echo "all symlink conversions done, checking if there are any bad symlinks with fslint/findbl now:" ; echo ""
  links=$(/usr/share/fslint/fslint/findbl -A "$1")
  if [ "$links" ];then
    echo "ERROR! - There are some absolute links left!?:"
    echo "$links" ; echo ""
  fi
  links=$(/usr/share/fslint/fslint/findbl -d "$1")
  if [ "$links" ];then
    echo "ERROR! - There are some dangling links!?:"
    echo "$links" ; echo ""
  fi
  links=$(/usr/share/fslint/fslint/findbl -s "$1")
  if [ "$links" ];then
    echo "ERROR! - There are some suspect links!?:"
    echo "$links" ; echo ""
  fi
  links=$(/usr/share/fslint/fslint/findbl -n "$1")
  if [ "$links" ];then
    echo "ERROR! - There are some links with redundant infos!?:"
    echo "$links" ; echo ""
  fi
}

prepare_first() {
  echo "extracting first backup ($1) and preparing it for merging"
  extract "$1"
  restructure_backup "$extracted"

  echo "keep a copy of first avatar dir before converting duplicates to symlinks, to be able to use that for integration runs of further backups."
  avatar_bak="$(basename ${newavatar})_nolinks"
  cp --recursive "$newavatar" "$avatar_bak"
  kept_avatar=1
  convert_dups_to_symlinks_and_check "$extracted"
  mv --verbose --no-clobber "$extracted" "signal-backups_merged"
  target="signal-backups_merged"
}

tryApply() {
  rm -f -- "$testdb"
  cp --verbose --no-clobber "$db1_path" "$testdb"
  applyOutput=$(cat "$diff" | sqlite3 "$testdb" 2>&1)
  if [ ! -z "$applyOutput" ]; then
    echo -e "${YELLOW}WARNING: Applying patch produced output: [$applyOutput]${NC}"
  fi
  missing=$(sqldiff "$testdb" "$db")
}

tryPatch() {
  db1_path="$1"
  db2_path="$2"
  db1=$(basename "$db1_path")
  db2=$(basename "$db2_path")
  echo "processing database comparison: $db1 -> $db2"
  diff="$3/signal_backup_${db1:14:19}to${db2:14:19}.sql"
  sqldiff "$db1_path" "$db2_path" > "$diff"
  db2_size=$(stat -c%s "$db")
  diff_size=$(stat -c%s "$diff")

  if [[ $preferSize -eq 1 && $diff_size -gt $db2_size ]]; then
    echo -e "${BLUE}patch is larger than db2, keeping original db2 and deleting patch: $diff_size > $db2_size${NC}"
    rm --verbose "$diff"
    #mv --verbose "$db" "$target/"
    return
  fi

  echo "patch is smaller than db2 (or we prefer patches over size), checking if it suffices to transform (a copy of) db1 into db2"
  tryApply
  while [[ ! -z "$missing" && "$missing" != "$missing_prev" ]]; do
    echo "there are some differences left"

    # https://stackoverflow.com/questions/6348902/how-can-i-add-numbers-in-a-bash-script
    diff_size=$(($diff_size + ${#missing}))
    if [[ $preferSize -eq 1 && $diff_size -gt $db2_size ]]; then
      echo -e "${BLUE}adding the differences to the patch makes it larger than db2. will instead keeping original db2 and delete the patch: $diff_size > $db2_size${NC}"
      rm --verbose "$diff"
      #mv --verbose "$db" "$target/"
      return
    fi

    echo "$missing" >> "$diff"
    missing_prev="$missing"

    tryApply
  done

  if [ -z "$missing" ]; then
    echo "no difference left, patch is good, keeping it."
    return
  else
    echo -e "${RED}patch application failed, there's still a difference to target! try creating a patch against previous database!${NC}" # deleting patch and keeping original target db
    #echo -e "${PURPLE}$missing${NC}"
    rm --verbose "$diff"

    if [[ "$previousdb" == *"signal-backups_merged/databases_processed/"* ]]; then
      echo "we are already comparing against an older database, see if there is an even older one we can try"
      previousdb="signal-backups_merged/databases_processed/$(ls signal-backups_merged/databases_processed | grep -B 1 "$db1" | head -n 1)"
    elif [[ "$previousdb" == *"signal-backups_merged/database_patches/"* ]] ; then
      echo "we are already comparing against one of the unpatchable databases, see if there is an even older one we can try"
      previousdb="$(ls signal-backups_merged/database_patches/signal_backup_*.db | grep -B 1 "$db1" | head -n 1)"
    else
      echo "lets try against the newest database from signal-backups_merged/databases_processed/"
      previousdb="signal-backups_merged/databases_processed/$(ls signal-backups_merged/databases_processed | tail -n 1)"
    fi

    if [ "$previousdb" == "$db1_path" ]; then
      if [[ "$previousdb" == *"signal-backups_merged/databases_processed/"* ]]; then
        echo "cant find any older dbs in signal-backups_merged/databases_processed/, lets try signal-backups_merged/databases_patches/"
        previousdb="$(ls signal-backups_merged/database_patches/signal_backup_*.db | tail -n 1)"
      elif [[ "$previousdb" == *"signal-backups_merged/database_patches/"* ]] ; then
        echo "failed to find any older databases we can try to patch against, giving up and keeping the database as unpatchable in the patch directory"
        return 1
      else
        echo "unknown state, please debug & fix code!"
        return 2
      fi
    fi
    tryPatch "$previousdb" "$2" "$3"
    return "$?"
    #read -n 1 -s -r -p "Press any key to continue"
    #mv --verbose "$db" "$target/"
  fi
  return 0
}

database_patches() {
  echo "generating dabase patches"
  patch_dir="$1/database_patches"
  mkdir -p "$patch_dir"
  reference=""
  for db in "$1/signal_backup_"*".db"; do
    if [ "$reference" ];then
      tryPatch "$reference" "$db" "$patch_dir"
      check=$(basename "$reference")
      check=${check#signal_backup_}
      check=${check%.db}
      if ! compgen -G "$patch_dir/*${check}.*" > /dev/null; then
        # if a file with the timestamp of $reference right before the extension doesn't exist in database_patches
        # it means neither the original database, nor an update-script to reach it is there yet.
        # that means we need to keep the reference database to have an unbroken chain
        mv --verbose "$reference" "$patch_dir"
      fi
    fi
    if [ -f "$reference" ]; then
      mkdir -p "$1/databases_processed"
      mv --verbose --no-clobber "$reference" "$1/databases_processed/$(basename "$reference")"
    fi
    reference="$db"
  done
}

extract_and_merge() {
  echo "merging $archive into previously integrated folder"
  extract "$1"
  restructure_backup "$extracted"

  if ! [ -d "$newavatar" ];then
    echo "couldn't find new avatar folder, seems to be an incomplete backup. will merge what we got."
    echo "$newavatar"
    kept_avatar=0
  else
    echo "comparing avatar folders:"
    echo "$3"
    echo "$newavatar"
    #~/bin/unison -watch=false -perms 0 -diff "~/Nextcloud/Scripts/nemo-scripts/diff-images" "$3" "$newavatar"
    if [ $(ls "$3" | wc -l) -ne $(ls "$newavatar" | wc -l) ];then
      echo "different number of files"
      kept_avatar=1
    else
      echo "same number of files"
      diff=$(diff -r "$3" "$newavatar" 2>&1 | head -c 1000)
      if [ "$diff" ] ; then
        echo "but different content:"
        echo "$diff"
        kept_avatar=1
      else
        echo "and same content. remove duplicate and rename previous folder to signify it stands for both folders"
        rm --recursive "$newavatar" # --verbose
        prev_avatar=$(ls "$2" | grep avatar | tail -n 1)
        if [[ "$prev_avatar" =~ ^.*\+[0-9]+$ ]];then
          echo "previous folder already has a +n suffix, increment instead of adding another +1"
          add=${prev_avatar##*+}
          ((add++))
          echo "${prev_avatar%+*}+$add"
          mv --verbose --no-clobber "$2/$prev_avatar" "$extracted/${prev_avatar%+*}+$add"
        else
          mv --verbose --no-clobber "$2/$prev_avatar" "$extracted/$prev_avatar+1"
        fi
        kept_avatar=0
      fi
    fi
  fi
  echo ""

  if [ $kept_avatar -eq 1 ];then
    echo "since the current avatar dir is different from previous one, we keep it and save a copy of it before converting duplicates to symlinks, to be able to use that for integration runs of further backups, also delete previous avatar backup since it's no longer needed."
    cp --recursive "$newavatar" "$(basename ${newavatar})_nolinks" # --verbose # too many files, clutters terminal
    rm -r "$3"
    echo ""
    avatar_bak="$(basename ${newavatar})_nolinks"
  else
    avatar_bak="$3"
  fi

  echo "move new stuff from fresh extracted folder into previous = target dir"
  mv --no-clobber "$extracted/avatar"* "$2/"
  mv --no-clobber "$extracted/signal_backup"* "$2/"
  mv --no-clobber "$extracted/preference" "$2/"
  mv --no-clobber "$extracted/1_sticker" "$2/"
  mv --no-clobber "$extracted/1_sticker/"* "$2/1_sticker/"
  mv --no-clobber "$extracted/2_attachment" "$2/"
  mv --no-clobber "$extracted/2_attachment/"* "$2/2_attachment/"
  mv --no-clobber "$extracted/preference" "$2/"
  mv --no-clobber "$extracted/preference/"* "$2/preference/"

  database_patches "$2"
  echo ""

  echo "now we'll check if there's anything left in previous backup that's not also in the new integrated version (there shouldn't be)" # --verbose
  diff=$(diff --recursive "$extracted" "$2" 2>&1 | grep --invert-match "Only in $2") #  | grep --invert-match "$extracted/.*: No such file or directory"

  if [ "$diff" ];then
    echo "found unexpected differences between previous backup and newly integrated version, printing diff result and opening unison gui to visualize differences:"
    echo "$diff"
    ~/bin/unison -watch=false -perms 0 -diff "~/Nextcloud/Scripts/nemo-scripts/diff-images" "$2" "$extracted" # -ignorearchives
  else
    echo "there's nothing left in the new backup that we haven't integrated into previous = target folder, deleting new extracted backup folder now."
    rm --recursive "$extracted"
  fi
  echo ""

  convert_dups_to_symlinks_and_check "$2"
}

parameterError() {
  echo "problems with parameters, please adapt parameters or adjust implementation"
  echo "Give a single parameter = path to all your unextracted backups, to start a fresh extraction."
  echo "Give 3 parameters to continue merging new backups into already extracted backup: 1 = folder with new backups (or single backup), 2 = previous backup, 3 = avatar folder to check if it changed"
  exit 8
}

############################## above = functions, below = "main" ##############################

pwd="$(pwd)"

if [ $# -eq 1 ] ; then
  if [ -d "$1" ]; then
    echo "got single parameter, this mode is to start a fresh extraction. Use this if you don't have any already extracted backups."
    first=1
    for archive in "$1/signal-"*".backup"; do
      if [ $first -eq 1 ]; then
        prepare_first "$archive"
        first=0
      else
        extract_and_merge "$archive" "$target" "$avatar_bak"
      fi
      #target="$extracted" # keep previous directory as target, remove new one instead.
    done
  else
    parameterError
  fi
elif [ $# -eq 3 ] ; then
  target="$2"
  avatar_bak="$3"
  if [ -d "$1" ]; then
    for archive in "$1/signal"*".backup"; do
      extract_and_merge "$archive" "$target" "$avatar_bak"
      #target="$extracted" # keep previous directory as target, remove new one instead.
    done
  else if [ -f "$1" ]; then
    extract_and_merge "$1" "$target" "$avatar_bak"
  else
    parameterError
  fi fi
else
  parameterError
fi
