# signal-backup-merging

bash script that uses https://github.com/pajowu/signal-backup-decode to extract signal-backups and merges them together, preserving the history and replacing duplicates with relative symbolic links. Also replaces db-backups by sqldiff scripts.

## Getting started

Save the bash script locally (somewhere secure) and modify the first function called "extract()" so that it will be able to extract your backups (requires passphrases).

Then run it with a single parameter = the folder containing your signal backups.

If you want to continue merging more backups into an already existing extracted & merged backup folder call the script with 3 parameters:
1. new backup or folder with new backups
2. pre existing merged backup folder
3. avatar backup without symlinks (= the "other" remaining output folder from every run of this script)

## Support / Contributing

Please use the issue tracker, file pull requests, create forks and so on. Do whatever you like and is permissible under the given license:

## License

https://www.gnu.org/licenses/agpl-3.0.en.html

## Authors and acknowledgment

The bash-script is just me. The tools used within the script not me, e.g.:
- https://github.com/pajowu/signal-backup-decode

## Project status

I think I've "finished" this tool, at least "it works for me". If you find it useful, I'd love to hear about it. Please feel free to contact me, probably the easiest way is to use the issue tracker included here on gitlab.
